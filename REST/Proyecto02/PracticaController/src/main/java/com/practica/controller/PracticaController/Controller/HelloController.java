package com.practica.controller.PracticaController.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class HelloController {
    
    @GetMapping("/hello")
    public String saludar(){
        return "Hola a todos, esta API reliza operaciones matematicas";
    }

}
