package com.practica.controller.PracticaController.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/operaciones")
public class OperacionesController {
    
    @GetMapping("/suma")
    @ResponseBody
    public int suma(@RequestParam int a, @RequestParam int b){
        return a + b;
    }

    @GetMapping("/Resta")
    @ResponseBody
    public int Resta(@RequestParam int a, @RequestParam int b){
        return a - b;
    }
    @GetMapping("/Divicion")
    @ResponseBody
    public int Divicion(@RequestParam int a, @RequestParam int b){
        return a / b;
    }

    @GetMapping("/Multiplicacion")
    @ResponseBody
    public int multiplicacion(@RequestParam int a, @RequestParam int b){
        return a * b;
    }
}
