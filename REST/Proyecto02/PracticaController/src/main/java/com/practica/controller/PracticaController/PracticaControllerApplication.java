package com.practica.controller.PracticaController;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticaControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticaControllerApplication.class, args);
	}

}
