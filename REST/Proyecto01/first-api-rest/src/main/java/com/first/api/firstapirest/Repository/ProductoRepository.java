package com.first.api.firstapirest.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.first.api.firstapirest.model.Producto;

@Repository
public class ProductoRepository {

    private List<Producto> productos = new ArrayList<>();

    ///Crear producto o agregar valores a la lista 
    public void createProducto(){
        productos = List.of(
            new Producto(1,  "porducto 01 ",  11 , 1000), 
            new Producto(2,  "porducto 02 ",  12 , 2000), 
            new Producto(3,  "porducto 03 ",  13 , 3000), 
            new Producto(4,  "porducto 04 ",  14 , 4000), 
            new Producto(5,  "porducto 05 ",  15 , 5000), 
            new Producto(6,  "porducto 06 ",  16 , 6000), 
            new Producto(7,  "porducto 07 ",  17 , 7000), 
            new Producto(8,  "porducto 08 ",  18 , 8000), 
            new Producto(9,  "porducto 09 ",  19 , 9000), 
            new Producto(10, "porducto 010",  110, 10000), 
            new Producto(11, "porducto 011",  111, 11000)
        );
    }
    //listar todo los productos
    public List<Producto> getAllProductos(){
        return productos;
    }

    ///buscar un producto
    public Producto finById(int id){
        for(int i = 0; i <productos.size(); i++ ){
            if(productos.get(i).getId() == id){
                return productos.get(i);
            }
        }
        return null;
    }
    ///Buscar

    public List<Producto> search(String nombre){
        return productos.stream()
        .filter(x -> x.getNombre().startsWith(nombre))
        .collect(Collectors.toList());
    }

    public Producto save(Producto p){
        Producto producto  = new Producto();
        producto.setId(p.getId());
        producto.setNombre(p.getNombre());
    }

}
